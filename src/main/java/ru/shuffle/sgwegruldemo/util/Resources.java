package ru.shuffle.sgwegruldemo.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Resources {

    private static final Logger LOGGER = LoggerFactory.getLogger(Resources.class);

    private Resources() {
    }

    public static InputStream getAsStream(String resourcePath) throws IOException {
        return Resources.class.getResourceAsStream(resourcePath);
    }

    public static String getAsString(String resourcePath) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader r = new BufferedReader(
            new InputStreamReader(getAsStream(resourcePath), StandardCharsets.UTF_8))) {
            String l;
            while ((l = r.readLine()) != null) {
                sb.append(l).append('\n');
            }
        }
        catch (IOException ex) {
            LOGGER.error("Невозможно получить ресурс: '{}'", resourcePath, ex);
            return null;
        }

        return sb.toString();
    }

    public static String getAsString(Path filePath) {
        StringBuilder sb = new StringBuilder();
        try {
            Files.lines(filePath, StandardCharsets.UTF_8).forEach((l) -> {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(l);
            });
        } catch (IOException ex) {
            LOGGER.error("Невозможно прочитать файл: '{}'", filePath, ex);
            return null;
        }

        return sb.toString();
    }

    public static Properties getAsProperties(Path filePath) {
        Properties props = new Properties();
        
        try (Reader r = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)) {
            props.load(r);    
        } catch (IOException ex) {
            LOGGER.error("Невозможно прочитать файл: '{}'", filePath, ex);
            return null;            
        }
        
        return props;
    }

    public static void create(Path filePath, String content) {
        try {
            Files.write(filePath, Collections.singleton(content), StandardOpenOption.CREATE_NEW);
        } catch (IOException ex) {
            LOGGER.error("Невозможно создать файл: '{}'", filePath, ex);
        }
    }

    public static void create(Path filePath, byte[] content) {
        try {
            Files.write(filePath, content, StandardOpenOption.CREATE_NEW);
        } catch (IOException ex) {
            LOGGER.error("Невозможно создать файл: '{}'", filePath, ex);
        }
    }

    public static boolean exists(Path filePath) {
        return Files.exists(filePath);
    }

    public static void delete(Path filePath) {
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException ex) {
            LOGGER.error("Невозможно удалить файл: '{}'", filePath, ex);
        }
    }

}
