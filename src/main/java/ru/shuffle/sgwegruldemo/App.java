package ru.shuffle.sgwegruldemo;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.shuffle.sgwegruldemo.util.Resources;
import ru.shuffle.smev.gateway.client.SmevGatewayClient;
import ru.shuffle.smev.gateway.client.dto.AttachDto;
import ru.shuffle.smev.gateway.client.dto.MessageDto;
import ru.shuffle.smev.gateway.client.dto.MessageRequestDto;
import ru.shuffle.smev.gateway.client.dto.NotificationDto;
import ru.shuffle.smev.gateway.client.dto.ResponseDto;

public final class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private static final String CONF_DIR ="conf/";
    private static final String DATA_DIR ="data/";

    private static final String SGW_ENDPOINT;
    private static final String SGW_CLIENT_TOKEN;
    private static final String SGW_CLIENT_MNEMONICS;

    static {
        Properties props = Resources.getAsProperties(Paths.get(CONF_DIR, "application.properties"));

        SGW_ENDPOINT = props.getProperty("SMEV_GATEWAY_ENDPOINT");
        SGW_CLIENT_TOKEN = props.getProperty("SMEV_GATEWAY_CLIENT_TOKEN");
        SGW_CLIENT_MNEMONICS = props.getProperty("SMEV_GATEWAY_CLIENT_MNEMONICS");
    }

    private SmevGatewayClient sgwClient;

    public static void main(String... args) {
        if (args.length != 1) {
            System.out.println("Укажите аргумент:\n"
                             + "  -s, --send      отпарвка сообщения в СМЭВ\n"
                             + "  -r, --receive   получение ответов из СМЭВ");
            System.exit(0);
        }

        final String arg = args[0];

        switch(arg) {
            case "-s":
            case "--send": {
                App app = new App();
                app.send("egrul.req.1.xml");
                app.send("egrul.req.2.xml");
                app.send("egrul.req.3.xml");
                app.send("egrul.req.4.xml");
                app.send("egrul.req.5.xml");
                app.send("egrul.req.6.xml");
                System.exit(0);
            }
            case "-r":
            case "--receive": {
                new App().recieve();
                System.exit(0);
            }
            default: {
                System.out.println(String.format("Неизвестный аргумет - '%s'!", arg));
                System.exit(1);
            }
        }
    }

    private SmevGatewayClient getSgwClient() {
        if (this.sgwClient == null) {
            this.sgwClient = new SmevGatewayClient(SGW_ENDPOINT);
        }

        return this.sgwClient;
    }

    public void send(String xmlReqFile) {
        LOGGER.info("Отправка в СМЭВ сообщения из '{}'", xmlReqFile);

        String xmlReq = Resources.getAsString(Paths.get(DATA_DIR, xmlReqFile));
        MessageRequestDto messageRequestDto = newMessageRequestDto(xmlReq);
        // TODO: присоединить вложения

        ResponseDto<MessageDto> response = this.getSgwClient().createMessageRequest(
                SGW_CLIENT_TOKEN, SGW_CLIENT_MNEMONICS, null, messageRequestDto);

        MessageDto message = getResponseResult(response);
        if (message != null) {
            LOGGER.info("Создано сообщение запроса (messageId={}): {}", message.id, toString(message));
            Resources.create(getMessageIdPath(message.id), xmlReqFile);
        } else {
            LOGGER.error("Ошибка создания сообщения запроса!");
            System.exit(1);
        }
    }

    private Path getMessageIdPath(long messageId) {
        return Paths.get(DATA_DIR, String.valueOf(messageId));
    }

    private static MessageRequestDto newMessageRequestDto(String messagePrimaryContent) {
        MessageRequestDto dto = new MessageRequestDto();
        dto.messagePrimaryContent = messagePrimaryContent;

        return dto;
    }

    private static <T> T getResponseResult(ResponseDto<T> response) {
        if ((response != null) && (response.responseCode == 200)) {
            LOGGER.info("Статус ответа от smev-gateway - {}", response.responseCode);
            if (response.success) {
                LOGGER.info("Сообщение успешно отправлено в smev-gateway");
            } else {
                LOGGER.warn("Сообщение отправлено в smev-gateway, но получен ответ об ошибке: {}", response.toString());
            }

            return response.result;
        }

        LOGGER.error(toString(response));
        if (response.error != null) {
            LOGGER.error("Проблема с форматом ответа от smev-gateway: errorCode={}, errorMessage={}",
                      response.error.code, response.error.message);
        } else {
            LOGGER.error("Проблема с форматом ответа от smev-gateway.");
        }

        return null;
    }


    public void recieve() {
        System.out.println("Получение сообщений из СМЭВ...");

        final int offset = 0;
        final int limit = 10;
        ResponseDto<NotificationDto[]> response = this.getSgwClient().listNotifications(
                SGW_CLIENT_TOKEN, SGW_CLIENT_MNEMONICS, null, limit, offset);

        if ((response.result == null) || (response.result.length == 0)) {
            LOGGER.info("Сообщения отсутствуют");
            return;
        }

        NotificationDto[] notifications = getResponseResult(response);
        if (notifications == null) {
            LOGGER.error("Проблема с форматом ответа от smev-gateway");
            System.exit(1);
        }

        LOGGER.info("Получено нотификаций: {} ", notifications.length);

        LOGGER.info("Обработка нотификаций");
        for (NotificationDto notification : notifications) {
            LOGGER.info(toString(notification));
            try {
                final boolean isNotificationProcessed = processNotification(notification);
                if (isNotificationProcessed) {
                    LOGGER.info("Подтверждение получения нотификации (id={})", notification.id);
                    getResponseResult(this.getSgwClient().ackNotification(
                            SGW_CLIENT_TOKEN, SGW_CLIENT_MNEMONICS, null, notification.id));
                } else {
                    LOGGER.info("Нотификация {} - пропущена", notifications.length);
                }
            } catch(Exception ex) {
                LOGGER.error("Ошибка обработки нотификации (id={})", notification.id, ex);
            }
        }

    }

    boolean processNotification(NotificationDto notification) {

        MessageDto message = getResponseResult(this.getSgwClient().findMessageById(
                SGW_CLIENT_TOKEN, SGW_CLIENT_MNEMONICS, null, notification.messageId));
        if (message == null) {
            LOGGER.error("Проблема с форматом ответа от smev-gateway");
            return false;
        }

        if ((message.originalMessageId == null) || (! Resources.exists(getMessageIdPath(message.originalMessageId)))) {
            if (! Resources.exists(getMessageIdPath(notification.messageId))) {
                return false;
            }
        }

        LOGGER.info("Получена нотификация типа '{}' с сообщением: {}", notification.type, toString(message));

        switch (notification.type) {
            case SMEV_ACCEPT: {
                // Здесь можно помечать, что запрос ушёл в СМЭВ
                return true;
            }
            case SMEV_SEND_ERROR: {
                // Запрос не ушёл, к примеру по причине отсутствия соединения с SGW
                LOGGER.error("Ошибка отправки сообщения (messageId={}) в СМЭВ", message.id);
                Resources.delete(getMessageIdPath(message.id));
                return true;
            }

            case SMEV_REQUEST: {
                // Нерелевантно для случая запроса в ЕГРЮЛ
                return false;
            }
            case SMEV_RESPONSE: {
                final String reqFileName = Resources.getAsString(getMessageIdPath(message.originalMessageId));
                final String respSoapFileName = reqFileName.replace("req", "resp").
                                                            replace("xml", "soap.xml");
                Resources.create(Paths.get(DATA_DIR, respSoapFileName), message.responseSoap);
                if (message.attaches != null) {
                    for (AttachDto attach : message.attaches) {
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        if (this.getSgwClient().downloadAttachContent(SGW_CLIENT_TOKEN, SGW_CLIENT_MNEMONICS, null, attach.id, outputStream).success) {
                            final String respAttchFileName = reqFileName.replace("req", "resp")
                                                                        .replace("xml", "attch." + attach.id);
                            Resources.create(Paths.get(DATA_DIR, respAttchFileName), outputStream.toByteArray());
                        } else {
                            LOGGER.error("Ошибка загрузки приложения (attachId={}) для сообщения (messageId={})", attach.id, message.id);
                        }
                    }
                }
                Resources.delete(getMessageIdPath(message.originalMessageId));
                return true;
            }
            case SMEV_ERROR: {
                final String reqFileName = Resources.getAsString(getMessageIdPath(message.id));
                final String errSoapFileName = reqFileName.replace("req", "err.soap");
                Resources.create(Paths.get(DATA_DIR, errSoapFileName), message.responseSoap);
                Resources.delete(getMessageIdPath(message.id));
                return true;
            }

            default: {
                LOGGER.warn("Отсутствует обработчик для сообщения типа {}", notification.type);
                return false;
            }
        }
    }
    
    private static String toString(ResponseDto<?> dto) {
        return "ResponseDto(success=" + dto.success +
             ", error=" + dto.error +
             ", result=" + dto.result +
             ", state=" + dto.state +
             ", mnemonics=" + dto.mnemonics +
             ", responseCode=" + dto.responseCode + ")";
    }

    private static String toString(MessageDto dto) {
        return "MessageDto(" +
               "createDate=" + dto.createDate +
             ", updateDate=" + dto.updateDate +
             ", sendedDate=" + dto.sendedDate +
             ", receivedDate=" + dto.receivedDate +
             ", originalMessageId=" + dto.originalMessageId +
             ", smevId=" + dto.smevId +
             ", smevOriginalMessageId=" + dto.smevOriginalMessageId +
             ", smevReferenceMessageId=" + dto.smevReferenceMessageId +
             ", type=" + dto.type +
             ", status=" + dto.status +
             ", mnemonics=" + dto.mnemonics +
             ", exchangeProtocolURN=" + dto.exchangeProtocolURN +
             ", requestJson=" + toTrimmedString(dto.requestJson, 75) +
             ", requestSoap=" + toTrimmedString(dto.requestSoap, 75) +
             ", responseSoap=" + toTrimmedString(dto.responseSoap, 75) +
             ", requestAckSoap=" + toTrimmedString(dto.requestAckSoap, 75) +
             ", responseAckSoap=" + toTrimmedString(dto.responseAckSoap, 75) +
             ", log=" + toTrimmedString(dto.log, 100) +
             ", attaches=" + (((dto.attaches != null) && (dto.attaches.length > 0)) ? "yes" : "no") + ")";
    }

    private static String toTrimmedString(Object obj, int length) {
        if (obj == null) {
            return null;
        }

        String str = Objects.toString(obj);
        if (str.length() <= length) {
            return str;
        }

        return str.substring(0, length - 3) + "...";
    }

    private static String toString(NotificationDto dto) {
        return "NotificationDto(id=" + dto.id +
             ", messageId=" + dto.messageId +
             ", exchangePorotocolURN=" + dto.exchangePorotocolURN +
             ", mnemonics=" + dto.mnemonics +
             ", createDate=" + dto.createDate +
             ", type=" + dto.type + ")";
    }

}
